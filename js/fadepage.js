// Fancy shmancy
jQuery.noConflict();
jQuery(function($) {
  $('#pageLoading').show();
  $(document).ready(function(){
    $('#pageLoading').fadeOut('slow');
  });
});
