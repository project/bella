// $Id$

function hb() {
  jQuery('#collapsibutton').slideUp("slow");
  // Hide msgsblock using an effect.
  jQuery('#msgs').slideUp("slow");
}

jQuery(document).ready(function() {
  var msgsblock = jQuery('#msgs');
  var text = Drupal.t('Dismiss');
  jQuery('#msgs').append('<div id="collapsibutton">' + text + '</div>');
  jQuery('#collapsibutton').css({
    'width': '90px',
    'border': 'solid',
    'border-width': '1px',
    'border-color': '#C5C5C5',
    'border-top': 'none',
    'color': '#CC0033',
    'margin-top': '-7px',
    'padding': '3px',
    'text-align': 'center',
    'cursor': 'pointer',
    'float': 'right',
    'clear': 'both',
    'background-color': '#DDDDDD'
  });

  // Add a handler that runs once when the Dismiss button is clicked.
  jQuery('#collapsibutton').one('click', function() {
    // Button clicked - hide messages.
    hb();
  });
});
