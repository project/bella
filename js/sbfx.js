// $Id$
jQuery.noConflict();
function sbfx() {
  jQuery('#search-block-form').css({
    'box-shadow': '0 1px 1px #000000',
    'margin-bottom': '-3px',
    'padding-top': '5px'
  });
}

jQuery(document).ready(function() {
  // Add a handler that runs once when the Search button is clicked.
  jQuery('#headertopright .form-submit').one('click', function() {
    // Button clicked - compress block.
    sbfx();
  });
});
