// $Id$

function ulbfx() {
  jQuery('#user-login-form').css({
    'box-shadow': '0 1px 1px #000000',
    'height': '44px',
    'margin-bottom': '-5px',
    'padding-top': '5px'
  });
}

jQuery(document).ready(function() {
  // Add a handler that runs once when the Login button is clicked.
  jQuery('#headertopright .form-submit').one('click', function() {
    // Button clicked - compress block.
    ulbfx();
  });
});
